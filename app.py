import falcon
import os

from dotenv import load_dotenv
load_dotenv()
from falcon_auth import FalconAuthMiddleware, JWTAuthBackend
from text_processing import TextProcessing as Nlp

auth_backend = JWTAuthBackend(
            lambda u:u,
            os.getenv('VDB_AUTH_SECRET'),
            algorithm='HS256',
            auth_header_prefix = 'Bearer')
auth_middleware = FalconAuthMiddleware(auth_backend)



class CORSMiddleware(object):
    def process_response(self, req, resp, resource, req_succeeded):
        resp.set_header('Access-Control-Allow-Origin', '*')

        if (req_succeeded
            and req.method == 'OPTIONS'
            and req.get_header('Access-Control-Request-Method')
        ):
            # NOTE(kgriffs): This is a CORS preflight request. Patch the
            #   response accordingly.

            allow = resp.get_header('Allow')
            resp.delete_header('Allow')

            allow_headers = req.get_header(
                'Access-Control-Request-Headers',
                default='*'
            )

            resp.set_headers((
                ('Access-Control-Allow-Methods', allow),
                ('Access-Control-Allow-Headers', allow_headers),
                ('Access-Control-Max-Age', '86400'),  # 24 hours
            ))

class NlpResource:
    def on_post(self, req, resp):
        """Handles GET requests"""
        print(req.media['text'])
        nlp = Nlp(req.media['text'])
        print(nlp.command)
        result = {
            'command': nlp.command
        }

        resp.media = result

api = falcon.API(middleware = [
    CORSMiddleware(),
    auth_middleware,
])
api.add_route('/', NlpResource())