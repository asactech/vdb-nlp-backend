import six
from enum import Enum
from google.cloud import language
from google.cloud.language import enums, types

class Cardinality(Enum):
    UNO = '1'
    MUCHOS = 'N'

class Mandatory(Enum):
    TOTAL = '1'
    PARCIAL = '0'

class TextProcessing:
    CREATE_ACTION_LABEL = 'agregar'
    UPDATE_ACTION_LABEL = 'modificar'
    DELETE_ACTIONS_LABEL = 'eliminar'
    ACTIONS = ['agregar', 'crear', 'añadir', 'modificar', 'cambiar', 'actualizar', 'editar', 'eliminar', 'borrar', 'quitar', 'descartar']
    KEYWORDS = ['entidad', 'atributo', 'relación', 'cardinalidad', 'obligatoriedad']
    SUB_KEYWORDS = ['compuesto', 'componer']
    FLEXIBLE_WORDS = ['quisiera', 'quiero', 'puedes', 'necesito', 'requiero', 'podrías']
    FLEXIBLE_COMPLEX_WORDS = ['por', 'me', 'yo', 'hacer', 'hacerme']
    FLEXIBLE_COMPLEX_CWORDS = ['por favor', 'me gustaría', 'yo quisiera', 'me puedes', 'hacer el favor de', 'hacerme el favor de', 'hacer me el favor de']
    CLIENT = language.LanguageServiceClient()

    def __init__(self, text):
        self.text = text.lower()
        self.identified_action = ''
        self.identified_target = ''
        self.command = {}
        self.entities_array = []
        self.attributes_array = []
        self.relations_array = []
        self.relations_cardinality_array = []
        self.relations_mandatory_array = []
        self.args = []
        self.index = 0

        if isinstance(self.text, six.binary_type):
            self.text = self.text.decode('utf-8')

        document = types.Document(
            content =   self.text,
            type =      enums.Document.Type.PLAIN_TEXT)

        self.tokens = self.CLIENT.analyze_syntax(document).tokens
        self.tokens_length = len(self.tokens)

        self.check_flexible_words()
        # self.identify_action()
        self.identify_model_parts()
        self.build_command()

    def check_flexible_words(self):
        word = ''
        while self.index < self.tokens_length:
            current_index = self.index
            # print('EXTERNO ', self.index)
            if self.tokens[self.index].text.content in self.FLEXIBLE_COMPLEX_WORDS:
                while self.index < self.tokens_length:
                    # print('INTERNO ', self.index, word)
                    if word in self.FLEXIBLE_COMPLEX_CWORDS:
                        word = ''
                        break
                    word = word if word == '' else word + ' '
                    word = word + self.tokens[self.index].text.content
                    self.index += 1
            if self.tokens[self.index].text.content in self.FLEXIBLE_WORDS:
                self.index += 1
            # if self.tokens[self.index].lemma in self.ACTIONS:
            #     self.identified_action = self.tokens[self.index].lemma
            #     self.index += 1
            #     break
            try:
                action_index = self.ACTIONS.index(self.tokens[self.index].lemma)
                if action_index <= 2:
                    self.identified_action = self.CREATE_ACTION_LABEL
                elif action_index <= 6:
                    self.identified_action = self.UPDATE_ACTION_LABEL
                else:
                    self.identified_action = self.DELETE_ACTIONS_LABEL

                self.index += 1
                break
            except ValueError:
                pass
            if current_index == self.index:
                print('Por favor, indique una acción.')
                break

    def identify_action(self):
        if self.tokens[self.index].lemma in self.ACTIONS:
            self.identified_action = self.tokens[self.index].lemma
            self.index += 1
        else:
            print('Por favor, indique una acción.')

    def identify_model_parts(self):
        while self.index < self.tokens_length:
            if self.tokens[self.index].lemma == self.KEYWORDS[0]:
                self.index += 1
                self.identified_target = self.KEYWORDS[0]
                self.build_entities()
                continue

            if self.tokens[self.index].lemma == self.KEYWORDS[1]:
                self.index += 1
                self.identified_target = self.KEYWORDS[1]
                self.build_attributes()
                continue

            if self.tokens[self.index].lemma == self.KEYWORDS[2]:
                self.index += 2
                self.identified_target = self.KEYWORDS[2]
                self.build_entities()
                self.build_relations()

            self.index += 1

    def build_entities(self):
        is_composite = self.pre_check_is_composite()
        composite_entity = ''

        while self.index < self.tokens_length:                
            if self.tokens[self.index].lemma in [self.KEYWORDS[1], self.KEYWORDS[3], self.KEYWORDS[4]]:
                break

            part_of_speech_tag = enums.PartOfSpeech.Tag(self.tokens[self.index].part_of_speech.tag)
            
            if part_of_speech_tag.name in ['NOUN', 'ADJ', 'VERB']:
                if is_composite:
                    composite_entity += self.tokens[self.index].lemma.upper() + '_'
                else:
                    self.entities_array.append({
                        'key': self.tokens[self.index].lemma.upper(),
                        'items': []})

            self.index += 1

        self.post_check_is_composite(is_composite, composite_entity, self.entities_array, 'key', 'items', [])

    def build_attributes(self):
        is_composite = self.pre_check_is_composite()
        composite_attribute = ''
    
        while self.index < self.tokens_length:
            if self.tokens[self.index].lemma == self.KEYWORDS[0]:
                break

            part_of_speech_tag = enums.PartOfSpeech.Tag(self.tokens[self.index].part_of_speech.tag)

            if part_of_speech_tag.name in ['NOUN', 'ADJ', 'VERB']:
                if is_composite:
                    composite_attribute += self.tokens[self.index].lemma + '_'
                else:
                    self.attributes_array.append({'name': self.tokens[self.index].text.content, 'iskey': False})

            self.index += 1

        self.post_check_is_composite(is_composite, composite_attribute, self.attributes_array, 'name', 'iskey', False)

    def build_relations(self):
        while self.index < self.tokens_length:
            if self.tokens[self.index].lemma == self.KEYWORDS[3]:
                self.index += 1
                self.identified_target = self.KEYWORDS[3]
                self.build_cardinality()
                break
            
            if self.tokens[self.index].lemma == self.KEYWORDS[4]:
                self.index += 1
                self.identified_target = self.KEYWORDS[4]
                self.build_mandatory()
            
            self.index += 1

        relation_dict = {
            'from': self.entities_array[0]['key'],
            'to': self.entities_array[1]['key']
        }
        if self.identified_target == self.KEYWORDS[2]:
            relation_dict['fromArrow'] = 'Line'
            relation_dict['toArrow'] = 'Line'
        self.relations_array.append(relation_dict)

        self.entities_array = []

    def build_cardinality(self):
        while self.index < self.tokens_length:
            part_of_speech_tag = enums.PartOfSpeech.Tag(self.tokens[self.index].part_of_speech.tag)

            if part_of_speech_tag.name == 'PRON' and self.tokens[self.index].lemma in ['uno', 'muchos']:
                self.relations_cardinality_array.append(Cardinality[self.tokens[self.index].lemma.upper()].value)

            self.index += 1

    def build_mandatory(self):
        while self.index < self.tokens_length:
            part_of_speech_tag = enums.PartOfSpeech.Tag(self.tokens[self.index].part_of_speech.tag)

            if part_of_speech_tag.name in ['NOUN', 'ADJ'] and self.tokens[self.index].lemma in ['parcial', 'total']:
                self.relations_mandatory_array.append(Mandatory[self.tokens[self.index].lemma.upper()].value)

            self.index += 1

    def build_command(self):
        self.command = {
            'action': self.identified_action,
            'target': self.identified_target
        }

        if self.entities_array:
            self.command['entities'] = self.entities_array
        
        if self.attributes_array:
            self.command['entities'][0]['items'] = self.attributes_array

        if self.relations_array:
            self.command['relations'] = self.relations_array

        if self.relations_cardinality_array:
            self.command['cardinality'] = self.relations_cardinality_array

        if self.relations_mandatory_array:
            self.command['mandatory'] = self.relations_mandatory_array

    def pre_check_is_composite(self):
        if self.tokens[self.index].lemma not in self.SUB_KEYWORDS:
            return False
        self.index += 1
        return True

    def post_check_is_composite(self, is_composite, composite_text, obj_array, *args):
        if is_composite and composite_text:
            composite_text = composite_text.strip('_')
            obj_array.append({args[0]: composite_text, args[1]: args[2]})

# tp = TextProcessing('agregar una relación entre carro y avión')
# print(tp.identified_action)
# print(tp.identified_target)
# print(tp.entities_array)
# print(tp.attributes_array)
# print(tp.relations_array)
# print(tp.relations_cardinality_array)
# print(tp.relations_mandatory_array)
# print(tp.args)
# print(tp.command)
